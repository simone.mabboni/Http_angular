import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map,share } from 'rxjs/operators';
import { TipoStazione } from '../domain/TipoStazione';
@Injectable({
  providedIn: 'root'
})
export class DatiService {
  private dato: number = 1;
  constructor(private http:HttpClient) { }
  getDato(){
    return this.dato;
  }
  getDatiLibro(){
    return this.http.get('https://raw.githubusercontent.com/benoitvallon/100-best-books/master/books.json');
  }

}
