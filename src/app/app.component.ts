import { Component,OnInit } from '@angular/core';
import { DatiService } from './services/dati.service';
import * as Rx from 'rxjs';
import { map } from 'rxjs/operators';
import { timer, Observable, Subject,of } from 'rxjs';
import { switchMap, takeUntil, catchError,flatMap  } from 'rxjs/operators';
import {TipoStazione } from './domain/TipoStazione';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'esempioservice';
  dato = 0;
  city = 'Inserire la città';
  d : Date;
  ora : Date;
  datiLibro: Libro[] = [];

  ngOnInit() {
    Rx.interval(155000)
    .pipe(
        flatMap(() => this.datiService.getDatiLibro())
    )
    .subscribe((dati: Libro[]) => {
      this.datiLibro = dati;
       this.d = new Date(this.datiLibro[0].data);
       this.ora= new Date();
       console.log(this.d.getTime());
       console.log(this.ora.getTime());
       console.log((this.ora.getTime()-this.d.getTime()));
    })
  }

  constructor(private datiService: DatiService) {}

  onClick() {
    this.getDatiLibro();
    console.log(this.country);
    this.dato = this.datiService.getDato();
  }

  getDatiLibro(){
    this.datiService.getDatiLibro().subscribe(
      (dati: Libro[]) => {
        this.datiLibro = dati;
         console.log(this.datiLibro);
         this.d = new Date(this.datiLibro[0].data);
         console.log(this.d.getTime());
      });
  }
  onGetCity(country: string) {
    console.log(country);
    this.datiService.getDatiLibro()
      .pipe(
        map((dati: Libro[]) => dati.filter(d => d.nome.indexOf(country) >= 0)))
      .subscribe(
        (dati: Libro[]) => this.datiLibro = dati
      );
  }


  temperaturaInf(year: number) {
    this.datiService.getDatiLibro()
      .pipe(s
        map((dati: Libro[]) => dati.filter(d => +d.year <= +year)))
      .subscribe(
        (dati: Libro[]) => this.datiLibro = dati
      );
    }
}
